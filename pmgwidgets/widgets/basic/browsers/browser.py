"""
代码来源：
https://www.cnblogs.com/taostaryu/p/9772492.html

"""
import sys
from qtpy.QtWidgets import QApplication, QWidget, QVBoxLayout, QToolBar, QLineEdit
from qtpy.QtCore import QUrl, Signal
from qtpy.QtWebEngineWidgets import QWebEngineView


class PMGWebBrowser(QWidget):
    def __init__(self, parent=None, toolbar='standard'):
        """

        :param parent:
        :param toolbar:多种选项：‘no’,‘standard’,'no_url_input','refresh_only'
        """
        super().__init__(parent)
        self.webview = PMGWebEngineView()
        self.webview.load(QUrl("https://cn.bing.com"))
        self.setLayout(QVBoxLayout())
        self.toolbar = QToolBar()
        self.url_input = QLineEdit()
        # self.url_input.setText('https://cn.bing.com')
        # self.load_url()
        self.toolbar.addWidget(self.url_input)
        self.toolbar.addAction('go').triggered.connect(lambda b: self.load_url())
        back_action = self.toolbar.addAction('back')
        back_action.triggered.connect(self.webview.back)

        forward_action = self.toolbar.addAction('forward')
        forward_action.triggered.connect(self.webview.forward)
        self.layout().addWidget(self.toolbar)
        if toolbar == 'no':
            self.toolbar.hide()
        elif toolbar == 'no_url_input':
            self.url_input.hide()
        elif toolbar == 'refresh_only':
            self.url_input.hide()
            back_action.setEnabled(False)
            forward_action.setEnabled(True)

        self.layout().addWidget(self.webview)
        self.setWindowTitle('My Browser')
        self.showMaximized()

        # command:>
        # jupyter notebook --port 5000 --no-browser --ip='*' --NotebookApp.token=''
        # --NotebookApp.password='' c:\users\12957\

        # self.webview.load(QUrl("http://127.0.0.1:5000/notebooks/desktop/Untitled.ipynb"))  # 直接请求页面。
        # self.webview.load(QUrl("E:\Python\pyminer_bin\PyMiner\bin\pmgwidgets\display\browser\show_formula.html"))  # 直接请求页面。
        # self.setCentralWidget(self.webview)

    def load_url(self, url: str = ''):
        if url == '':
            url = self.url_input.text().strip()
        else:
            self.url_input.setText(url)
        self.webview.load(QUrl(url))


class PMGWebEngineView(QWebEngineView):
    windowList = []
    signal_new_window_created = Signal(PMGWebBrowser)

    # 重写createwindow()
    def createWindow(self, QWebEnginePage_WebWindowType):
        new_window = PMGWebBrowser()
        self.windowList.append(new_window)  # 注：没有这句会崩溃！！！
        self.signal_new_window_created.emit(new_window)
        return new_window.webview


if __name__ == "__main__":
    app = QApplication(sys.argv)

    w = PMGWebBrowser()
    w.show()
    sys.exit(app.exec_())
