# PyMiner插件开发工具

PyMiner插件开发工具由应用工具栏applications_toolbar和插件开发包pmtoolbox组成。

pmtoolbox提供了一系列便捷接口如读写文件等。applications_toolbar则是有插件开发的指导。

## 新建应用的方式

![](doc_figures/应用工具栏.png)点击“应用开发”，即可进入设计界面。

还需要实现的功能：

- 应用开发部分，建议改在QWizard.validateCurrentPage方法里面，实现界面的输入检查逻辑函数。点击cancel的时候，如文件夹已经创建，则建议删除已创建的文件夹。
- 增加了创建完成后，文件树跳转到新建的文件夹的功能。
- 应用开发部分，选择图标时应该加一个默认值。
- 应用开发部分，增加一个界面模板库templates。并且在应用开发一栏中增加一个功能让用户选择是否创建ui文件并且通过ui文件来加载。另外，可以增加一个批量转换的功能，将当前文件夹下的.ui文件全部转换为对应的.py文件。这或许需要调用pyuic来做了。

## 项目的组织

### Windows系统下的组织

![image-20201112100723993](doc_figures\项目组成结构.png)

#### 简介

bin中存储我们的项目代码

interpreter中存储的是主解释器文件

attachments里面存储的是其他附加程序（深度定制的qtdesigner、其他解释器等等）

shared_site_packages存储的是一些纯python包，用来保存不同解释器之间可以共享的包，节约内存。

点击pyminer.cmd可以直接运行。

运行remove_caches_to_setup.py，可以移除项目下所有Python已生成的字节码，为项目瘦身。

