<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Form</name>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="85"/>
        <source>PyMiner</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../ui/base/aboutMe.ui" line="259"/>
        <source>侯展意
py2cn
Junruoyu-Zheng
心随风
nihk
cl-jiang
Irony
冰中火
houxinluo
开始说故事
...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="732"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="153"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="103"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="118"/>
        <source>1.0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../../ui/base/aboutMe.ui" line="183"/>
        <source>PyMiner is a mathematical tool based on Python. It provides solutions to various of problems by loading different plugins.  

Author：PyMiner Development Team
E-Mail：team@py2cn.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="194"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="229"/>
        <source>Credit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="752"/>
        <source>Donate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="156"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="199"/>
        <source>Python File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="249"/>
        <source>CSV File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="299"/>
        <source>Excel File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="349"/>
        <source>MATLAB File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="418"/>
        <source>Open Folder...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="496"/>
        <source>Quick Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="533"/>
        <source>Use Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="583"/>
        <source>Official Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="633"/>
        <source>Gitee Repo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="702"/>
        <source>Join Us</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../../app2.py" line="300"/>
        <source>Welcome to PyMiner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="306"/>
        <source>Plugs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="335"/>
        <source>Start Clear...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="337"/>
        <source>Clear all variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="474"/>
        <source>update status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="467"/>
        <source>You can download and install the updated version, whether to restart and install the update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="474"/>
        <source>Is the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="490"/>
        <source>Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="490"/>
        <source>You can give feedback through issue on suggestions or problems encountered in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="538"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="538"/>
        <source>Are you sure close?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="539"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="540"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PMToolBarHome</name>
    <message>
        <location filename="../../../app2.py" line="128"/>
        <source>New Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="132"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="137"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="142"/>
        <source>Get Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="146"/>
        <source>Import Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="150"/>
        <source>Load Var</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="150"/>
        <source>Save Var</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="150"/>
        <source>Clear Var</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="157"/>
        <source>Extensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="161"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="163"/>
        <source>Community</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="166"/>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="168"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="185"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="217"/>
        <source>Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="221"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="226"/>
        <source>Check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="230"/>
        <source>Give Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="234"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="14"/>
        <source>New Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="364"/>
        <source>Steps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="85"/>
        <source>1. Select Project Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="96"/>
        <source>2. Configure Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="214"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="219"/>
        <source>Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="263"/>
        <source>python-empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="272"/>
        <source>python-template-basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="281"/>
        <source>python-template-plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="290"/>
        <source>python-template-PyQt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="319"/>
        <source>Create a Python Project containing an empty main.py file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="382"/>
        <source>1. New Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="393"/>
        <source>2. Project Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="441"/>
        <source>Project Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="457"/>
        <source>Project Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="464"/>
        <source>demo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="475"/>
        <source>Project Directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="485"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="496"/>
        <source>Absolute Directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="159"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="203"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="255"/>
        <source>Project Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
