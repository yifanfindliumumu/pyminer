<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Form</name>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="85"/>
        <source>PyMiner</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="26"/>
        <source>Form</source>
        <translation>窗体</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="732"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="153"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="103"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="118"/>
        <source>1.0.1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="183"/>
        <source>PyMiner is a mathematical tool based on Python. It provides solutions to various of problems by loading different plugins.  

Author?PyMiner Development Team
E-Mail?team@py2cn.com</source>
        <translation type="obsolete">PyMiner一款基于数据空间的数学工具，通过加载各种插件实现不同的需求，用易于操作的形式，在统一的界面中，通过数据计算实现数据科学家所设想的任务

作者：PyMiner Development Team
邮箱：team@py2cn.com</translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="194"/>
        <source>System</source>
        <translation>系统信息</translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="229"/>
        <source>Credit</source>
        <translation>鸣谢</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="752"/>
        <source>Donate</source>
        <translation>捐赠</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="156"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="199"/>
        <source>Python File</source>
        <translation>Python文件</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="249"/>
        <source>CSV File</source>
        <translation>CSV文件</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="299"/>
        <source>Excel File</source>
        <translation>Excel文件</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="349"/>
        <source>MATLAB File</source>
        <translation>MATLAB文件</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="418"/>
        <source>Open Folder...</source>
        <translation>打开文件夹...</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="496"/>
        <source>Quick Start</source>
        <translation>快速启动</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="533"/>
        <source>Use Manual</source>
        <translation>使用说明</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="583"/>
        <source>Official Site</source>
        <translation>官方网站</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="633"/>
        <source>Gitee Repo</source>
        <translation>Gitee项目仓库</translation>
    </message>
    <message>
        <location filename="../../ui/base/first_form.ui" line="702"/>
        <source>Join Us</source>
        <translation>加入我们</translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="183"/>
        <source>PyMiner is a mathematical tool based on Python. It provides solutions to various of problems by loading different plugins.  

Author：PyMiner Development Team
E-Mail：team@py2cn.com</source>
        <translation>PyMiner一款基于数据空间的数学工具，通过加载各种插件实现不同的需求，用易于操作的形式，在统一的界面中，通过数据计算实现数据科学家所设想的任务

作者：PyMiner Development Team
邮箱：team@py2cn.com</translation>
    </message>
    <message>
        <location filename="../../ui/base/aboutMe.ui" line="259"/>
        <source>侯展意
py2cn
Junruoyu-Zheng
心随风
nihk
cl-jiang
Irony
冰中火
houxinluo
开始说故事
...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="app2.py" line="307"/>
        <source>Files</source>
        <translation type="obsolete">文件</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="300"/>
        <source>Welcome to PyMiner</source>
        <translation>欢迎使用PyMiner</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="306"/>
        <source>Plugs</source>
        <translation>插件</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="335"/>
        <source>Start Clear...</source>
        <translation>开始清除...</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="337"/>
        <source>Clear all variables</source>
        <translation>清除所有变量</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="474"/>
        <source>update status</source>
        <translation>更新状态</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="467"/>
        <source>You can download and install the updated version, whether to restart and install the update</source>
        <translation>你可以下载并安装升级后的版本，或者重启并安装更新</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="474"/>
        <source>Is the latest version</source>
        <translation>已是最新版本</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="490"/>
        <source>Feedback</source>
        <translation>反馈</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="490"/>
        <source>You can give feedback through issue on suggestions or problems encountered in use</source>
        <translation>你可以在这里提出反馈的意见和建议，我们将十分感谢！</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="538"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="538"/>
        <source>Are you sure close?</source>
        <translation>确定要关闭吗？</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="539"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="540"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>PMToolBarHome</name>
    <message>
        <location filename="../../../app2.py" line="128"/>
        <source>New Script</source>
        <translation>新建脚本</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="132"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="137"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="142"/>
        <source>Get Data</source>
        <translation>获取数据</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="146"/>
        <source>Import Database</source>
        <translation>从数据库导入</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="150"/>
        <source>Load Var</source>
        <translation>加载变量</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="150"/>
        <source>Save Var</source>
        <translation>保存变量</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="150"/>
        <source>Clear Var</source>
        <translation>清除变量</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="157"/>
        <source>Extensions</source>
        <translation>扩展中心</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="161"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="163"/>
        <source>Community</source>
        <translation>社区</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="166"/>
        <source>Layout</source>
        <translation>布局</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="168"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="217"/>
        <source>Support</source>
        <translation>官方网站</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="221"/>
        <source>Reference</source>
        <translation>帮助文档</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="226"/>
        <source>Check for updates</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="230"/>
        <source>Give Feedback</source>
        <translation>反馈</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="234"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="app2.py" line="219"/>
        <source>New Project</source>
        <translation type="obsolete">新建项目</translation>
    </message>
    <message>
        <location filename="app2.py" line="222"/>
        <source>Text Data</source>
        <translation type="obsolete">文本数据</translation>
    </message>
    <message>
        <location filename="app2.py" line="225"/>
        <source>CSV Data</source>
        <translation type="obsolete">CSV数据</translation>
    </message>
    <message>
        <location filename="app2.py" line="243"/>
        <source>Encode converter</source>
        <translation type="obsolete">编码转换</translation>
    </message>
    <message>
        <location filename="../../../app2.py" line="185"/>
        <source>Home</source>
        <translation>主页</translation>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="14"/>
        <source>New Project</source>
        <translation>新建项目</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="364"/>
        <source>Steps</source>
        <translation>步骤</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="85"/>
        <source>1. Select Project Type</source>
        <translation>1.选择项目类型</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="96"/>
        <source>2. Configure Project</source>
        <translation>2.项目设置</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="214"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="219"/>
        <source>Python</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="263"/>
        <source>python-empty</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="272"/>
        <source>python-template-basic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="281"/>
        <source>python-template-plot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="290"/>
        <source>python-template-PyQt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="319"/>
        <source>Create a Python Project containing an empty main.py file.</source>
        <translation>创建一个Python项目，包含一个空的main.py文件。</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="382"/>
        <source>1. New Project</source>
        <translation>1、新建项目</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="393"/>
        <source>2. Project Configuration</source>
        <translation>2、项目设置</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="441"/>
        <source>Project Configuration</source>
        <translation>项目设置</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="457"/>
        <source>Project Name:</source>
        <translation>项目名称：</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="464"/>
        <source>demo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="475"/>
        <source>Project Directory:</source>
        <translation>项目路径：</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="485"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="496"/>
        <source>Absolute Directory:</source>
        <translation>绝对路径：</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="159"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="203"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="../../ui/base/project_wizard.ui" line="255"/>
        <source>Project Type</source>
        <translation>项目类型</translation>
    </message>
</context>
</TS>
