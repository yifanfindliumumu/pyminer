<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="zh_CN" sourcelanguage="">
<context>
    <name>DialogGoto</name>
    <message>
        <location filename="../codeeditor/ui/ui_gotoline.py" line="45"/>
        <source>Go to Line/Column</source>
        <translation>前往行/列</translation>
    </message>
    <message>
        <location filename="../codeeditor/ui/ui_gotoline.py" line="46"/>
        <source>[Line] [:column]:</source>
        <translation>[行] [:列]:</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="258"/>
        <source>Text to Find</source>
        <translation>要查找的文本</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="259"/>
        <source>Text to Replace</source>
        <translation>替换为</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="260"/>
        <source>Wrap</source>
        <translation>循环查找</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="261"/>
        <source>Regex</source>
        <translation>匹配正则表达式</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="262"/>
        <source>Case Sensitive</source>
        <translation>大小写敏感</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="263"/>
        <source>Whole Word</source>
        <translation>匹配整个文字</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="268"/>
        <source>Up</source>
        <translation>向上</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="269"/>
        <source>Down</source>
        <translation>向下</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="270"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="271"/>
        <source>Replace All</source>
        <translation>替换全部</translation>
    </message>
</context>
<context>
    <name>FormEditor</name>
    <message>
        <location filename="../codeeditor/ui/ui_formeditor.py" line="51"/>
        <source>Length:{0}  Lines:{1}</source>
        <translation>长度:{0} 行{1}</translation>
    </message>
    <message>
        <location filename="../codeeditor/ui/ui_formeditor.py" line="52"/>
        <source>UTF-8</source>
        <translation>UTF-8</translation>
    </message>
    <message>
        <location filename="../codeeditor/ui/ui_formeditor.py" line="53"/>
        <source>Sel:{0} | {1}</source>
        <translation>选中区域：{0} | {1}</translation>
    </message>
    <message>
        <location filename="../codeeditor/ui/ui_formeditor.py" line="54"/>
        <source>Ln:{0}  Col:{1}</source>
        <translation>行：{0} 列：{1}</translation>
    </message>
    <message>
        <location filename="../codeeditor/ui/ui_formeditor.py" line="55"/>
        <source>Unix(LF)</source>
        <translation>Unix(LF)</translation>
    </message>
    <message>
        <location filename="../codeeditor/ui/ui_formeditor.py" line="50"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PMAbstractEditor</name>
    <message>
        <location filename="../codeeditor/abstracteditor.py" line="156"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../codeeditor/abstracteditor.py" line="156"/>
        <source>Save file &quot;{0}&quot;?</source>
        <translation>是否保存文件 &quot;{0}&quot;?</translation>
    </message>
</context>
<context>
    <name>PMBaseEditor</name>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="454"/>
        <source>Ln:{0}  Col:{1}</source>
        <translation>行：{0} 列：{1}</translation>
    </message>
    <message>
        <location filename="../codeeditor/baseeditor.py" line="1171"/>
        <source>File %s was modified externally.Reload?</source>
        <translation>文件 %s 被外部更改，是否重新加载？</translation>
    </message>
</context>
<context>
    <name>PMCPPEditor</name>
    <message>
        <location filename="../codeeditor/cppeditor.py" line="61"/>
        <source>Function Help</source>
        <translation>获取函数帮助</translation>
    </message>
</context>
<context>
    <name>PMCodeEditTabWidget</name>
    <message>
        <location filename="../codeeditor/tabwidget.py" line="498"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../codeeditor/tabwidget.py" line="622"/>
        <source>Run: %s</source>
        <translation>运行：%s</translation>
    </message>
    <message>
        <location filename="../codeeditor/tabwidget.py" line="628"/>
        <source>Run Python Code inside %s</source>
        <translation>运行 %s 中的代码</translation>
    </message>
    <message>
        <location filename="../codeeditor/tabwidget.py" line="711"/>
        <source>Script</source>
        <translation>脚本</translation>
    </message>
    <message>
        <location filename="../codeeditor/tabwidget.py" line="836"/>
        <source>Editor</source>
        <translation>编辑器</translation>
    </message>
</context>
<context>
    <name>PMCythonEditor</name>
    <message>
        <location filename="../codeeditor/cythoneditor.py" line="58"/>
        <source>Compile to Library</source>
        <translation>编译为运行库</translation>
    </message>
    <message>
        <location filename="../codeeditor/cythoneditor.py" line="59"/>
        <source>Analyse Profile</source>
        <translation>生成代码分析报告</translation>
    </message>
</context>
<context>
    <name>PMDebugConsoleTabWidget</name>
    <message>
        <location filename="../debugger.py" line="80"/>
        <source>Terminate</source>
        <translation>终止</translation>
    </message>
    <message>
        <location filename="../debugger.py" line="80"/>
        <source>Process is running, Would you like to terminate this process?</source>
        <translation>进程正在运行，是否要终止此进程？</translation>
    </message>
    <message>
        <location filename="../debugger.py" line="21"/>
        <source>Debugger</source>
        <translation>调试器</translation>
    </message>
</context>
<context>
    <name>PMEditorToolbar</name>
    <message>
        <location filename="../toolbar.py" line="9"/>
        <source>New Script</source>
        <translation>新建脚本</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="12"/>
        <source>Open Script</source>
        <translation>打开脚本</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="15"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="17"/>
        <source>Find/Replace</source>
        <translation>查找/替换</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="20"/>
        <source>Toggle Comment</source>
        <translation>切换注释</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="20"/>
        <source>Goto Line</source>
        <translation>跳转到行</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="26"/>
        <source>Indent</source>
        <translation>增加缩进</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="26"/>
        <source>Dedent</source>
        <translation>减少缩进</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="30"/>
        <source>IPython</source>
        <translation>IPython运行</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="32"/>
        <source>Separately</source>
        <translation>独立运行</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="34"/>
        <source>Terminal</source>
        <translation>终端中运行</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="37"/>
        <source>Debug</source>
        <translation>调试</translation>
    </message>
    <message>
        <location filename="../toolbar.py" line="41"/>
        <source>Editor</source>
        <translation>编辑器</translation>
    </message>
</context>
<context>
    <name>PMMarkdownEditor</name>
    <message>
        <location filename="../codeeditor/markdowneditor.py" line="89"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../codeeditor/markdowneditor.py" line="89"/>
        <source>Save file &quot;{0}&quot;?</source>
        <translation>是否保存文件 &quot;{0}&quot;？</translation>
    </message>
</context>
<context>
    <name>PMPythonEditor</name>
    <message>
        <location filename="../codeeditor/pythoneditor.py" line="76"/>
        <source>Function Help</source>
        <translation>获取函数帮助</translation>
    </message>
    <message>
        <location filename="../codeeditor/pythoneditor.py" line="80"/>
        <source>Help In Console</source>
        <translation>控制台中获取帮助</translation>
    </message>
    <message>
        <location filename="../codeeditor/pythoneditor.py" line="84"/>
        <source>Go to Definition</source>
        <translation>跳转到定义</translation>
    </message>
    <message>
        <location filename="../codeeditor/pythoneditor.py" line="340"/>
        <source>Help</source>
        <translation>获取帮助</translation>
    </message>
    <message>
        <location filename="../codeeditor/pythoneditor.py" line="340"/>
        <source>Cannot get name.
Maybe There is:
1&#xe3;&#x80;&#x81;Syntax error in your code.
2&#xe3;&#x80;&#x81;No word under text cursor.</source>
        <translation>无法获取名称。
可能有以下错误：
1、代码中有语法错误
2、指针下没有文字。</translation>
    </message>
    <message>
        <location filename="../codeeditor/pythoneditor.py" line="443"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../codeeditor/pythoneditor.py" line="503"/>
        <source>Running Current Script Cell (Line %d to %d).</source>
        <translation>运行当前的脚本单元（%d行到%d行）。</translation>
    </message>
</context>
</TS>
