from pmgwidgets import create_icon, PMGToolBar


class PMEditorToolbar(PMGToolBar):

    def __init__(self):
        super(PMEditorToolbar, self).__init__()

        self.add_tool_button('button_new_script', self.tr('New Script'),
                             create_icon(':/color/source/theme/color/icons/script.svg'))

        self.add_tool_button(
            'button_open_script', self.tr('Open Script'), create_icon(':/color/source/theme/color/icons/open.svg'))

        self.add_tool_button('button_save', self.tr('Save'), create_icon(":/color/source/theme/color/icons/save.svg"))
        self.addSeparator()
        self.add_tool_button(
            'button_search', self.tr('Find/Replace'), create_icon(":/color/source/theme/color/icons/find_replace.svg"))

        self.add_buttons(2, ['button_comment', 'button_goto'], [self.tr('Toggle Comment'), self.tr('Goto Line')],
                         [":/color/source/theme/color/icons/annotation.svg",
                          ':/color/source/theme/color/icons/jump_line.svg'])

        self.get_control_widget('button_goto').setEnabled(True)

        self.add_buttons(2, ['button_indent', 'button_unindent'], [self.tr('Indent'), self.tr('Dedent')],
                         [":/color/source/theme/color/icons/indent_left.svg",
                          ":/color/source/theme/color/icons/indent_right.svg"])
        self.addSeparator()
        self.add_tool_button('button_run_script', self.tr('IPython'),
                             create_icon(':/color/source/theme/color/icons/run.svg'))
        self.add_tool_button('button_run_isolated', self.tr('Separately'),
                             create_icon(':/color/source/theme/color/icons/cmd.svg'))
        self.add_tool_button('button_run_in_terminal', self.tr('Terminal'),
                             create_icon(':/color/source/theme/color/icons/cmd.svg'))

        self.add_tool_button('button_debug', self.tr('Debug'),
                             create_icon(':/color/source/theme/color/icons/debug.svg'))

    def get_toolbar_text(self) -> str:
        return self.tr('Editor')
