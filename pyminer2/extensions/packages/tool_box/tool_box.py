import os
import sys
from PyQt5.Qt import *
from typing import Dict, Callable, List
from PyQt5.QtCore import Qt, QModelIndex
from PyQt5.QtGui import QPixmap, QIcon, QCloseEvent
from PyQt5.QtWidgets import QVBoxLayout, QWidget, QFileDialog, QPushButton, QHBoxLayout, QLineEdit
from .WrapPanel import WrapPanel
from pmgwidgets import PMDockObject

class ToolBox(WrapPanel,PMDockObject):
    def __init__(self):
        super(ToolBox, self).__init__()

        self.current_path = os.path.dirname(__file__)
        self.add_button("筛选", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("查找替换", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("排序", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("数据信息", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("列名处理", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("数据角色", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("数据分区", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("新增列", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("删除列", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("新增行", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("删除行", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("缺失值", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("抽样", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("转置", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("纵向合并", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("横向合并", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("连接", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("标准化数据", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)
        self.add_button("列出", os.path.join(self.current_path, 'source/plot.svg'), 80, 80, self.close)


if __name__ == '__main__':
    app=QApplication(sys.argv)
    main=ToolBox()
    main.show()
    sys.exit(app.exec())


