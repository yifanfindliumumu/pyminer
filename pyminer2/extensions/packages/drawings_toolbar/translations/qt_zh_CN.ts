<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>PMDrawingsToolBar</name>
    <message>
        <location filename="../main.py" line="227"/>
        <source>Drawings</source>
        <translation>绘图</translation>
    </message>
</context>
<context>
    <name>ToolbarBlock</name>
    <message>
        <location filename="../main.py" line="142"/>
        <source>Variable Selected</source>
        <translation>已选变量</translation>
    </message>
    <message>
        <location filename="../main.py" line="143"/>
        <source>No Variable</source>
        <translation>未选择变量</translation>
    </message>
</context>
</TS>
