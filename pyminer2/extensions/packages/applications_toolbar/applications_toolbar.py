import os
import shutil
import subprocess
import sys
from typing import TYPE_CHECKING, Callable, List
import json
import platform
from qtpy.QtCore import QSize, Qt, Signal
from qtpy.QtGui import QPixmap
from qtpy.QtWidgets import QHBoxLayout, QWidget, QSpacerItem, QToolButton, QSizePolicy, QWizard, QMessageBox, \
    QFileDialog, QFrame,QApplication
from pyminer2.extensions.extensionlib import pmwidgets
from pmgwidgets import PMGToolBar, create_icon, PMGToolBox, QIcon
from pyminer2.ui import base
from pyminer2.extensions.packages.applications_toolbar.ui.app_designer import Ui_Wizard

# from PyQt5.Qt import *

if TYPE_CHECKING:
    from pyminer2.extensions.extensionlib import extension_lib


class PMMenuToolPanel(QFrame):
    """
    面板控件，用于放置绘图按钮或其他插件按钮
    """

    def __init__(self):
        super(PMMenuToolPanel, self).__init__()
        self.setup_ui()

    def setup_ui(self):
        self.setMinimumSize(QSize(500, 85))
        self.setMaximumSize(QSize(16777215, 85))
        self.setObjectName("frame")
        self.hbox = QHBoxLayout()
        self.hbox.setContentsMargins(0, 0, 0, 0)
        self.hbox.setSpacing(0)

        self.widget_panel = QWidget()
        self.widget_panel.setStyleSheet("margin:1px;")
        self.widget_panel_hbox = QHBoxLayout()
        self.widget_panel_hbox.setContentsMargins(0, 0, 0, 0)
        self.widget_panel_hbox.setSpacing(10)
        self.widget_panel.setLayout(self.widget_panel_hbox)

        self.hspace = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.btn_down = QToolButton()
        self.btn_down.setObjectName("btn_tool_select")
        self.btn_down.setToolTip("查看更多")
        self.btn_down.setMinimumSize(QSize(25, 85))
        self.btn_down.setMaximumSize(QSize(25, 85))
        self.btn_down.setStyleSheet(
            "#btn_tool_select{border:1px solid rgb(189,189,189);border-top-left-radius:0px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:5px;background-color: rgb(230,230,230);padding:0px 0px 0px 0px;}#btn_tool_select:hover{background:lightgray;}")

        self.current_path = os.path.dirname(__file__)
        icon1 = QIcon()
        icon1.addPixmap(QPixmap(os.path.join(self.current_path, 'source/down.svg')), QIcon.Normal,
                        QIcon.Off)
        self.btn_down.setIcon(icon1)

        self.btn_down.setAutoRaise(True)

        # 添加按钮和弹簧到水平布局
        self.hbox.addWidget(self.widget_panel)
        self.hbox.addItem(self.hspace)
        self.hbox.addWidget(self.btn_down)
        self.setLayout(self.hbox)
        self.setFrameShape(QFrame.StyledPanel)
        self.setFrameShadow(QFrame.Raised)
        self.setLineWidth(1)
        self.setStyleSheet(
            "#frame{border:1px solid rgb(189,189,189);padding: -5px -10px 0px 20px;margin: 0px 0px 0px 0px;border-radius:5px;}")

        self.btn_down.clicked.connect(self.close)

        # [NOTE] 这部分代码我注释掉了，按钮接口改成了其他插件添加。其他插件调用插件接口的add_process_action方法，进行按钮的添加。：
        # 测试添加按钮
        # self.add_button("柱形图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("折线图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("饼图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("条形图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("面积图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("气泡图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("箱线图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("直方图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("雷达图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("热力图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("地图", os.path.join(self.current_path, 'source/default.png'), self.close)
        # self.add_button("组合图", os.path.join(self.current_path, 'source/default.png'), self.close)

    def add_button(self, btn_text: str, icon_path: str, btn_action: Callable) -> None:
        sub_widget = QToolButton()
        icon = QIcon()
        icon.addPixmap(QPixmap(icon_path), QIcon.Normal,
                       QIcon.Off)
        sub_widget.setIcon(icon)
        sub_widget.setIconSize(QSize(50, 40))
        sub_widget.setMinimumSize(QSize(85, 75))
        sub_widget.setMaximumSize(QSize(85, 75))
        sub_widget.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        sub_widget.setAutoRaise(True)
        sub_widget.setText(btn_text)
        self.widget_panel_hbox.addWidget(sub_widget)
        sub_widget.clicked.connect(btn_action)


class PMApplicationsToolBar(PMGToolBar):
    app_item_double_clicked_signal: 'Signal' = Signal(str)
    extension_lib: 'extension_lib' = None
    variable = None
    widgets = {}

    def __init__(self):
        super().__init__()
        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)

        src_path = os.path.join(os.path.dirname(__file__), 'source')
        self.open_qtdesigner_button = \
            self.add_tool_button('open_qtdesigner_button', self.tr('Designer'),
                                 icon=create_icon(os.path.join(src_path, 'qt-logo.png')))
        self.packup_app_button = \
            self.add_tool_button('packup_app_button', self.tr('Develop'),
                                 icon=create_icon(os.path.join(src_path, 'package.svg')))

        self.publish_app_button = \
            self.add_tool_button('publish_app_button', self.tr('Distribute'),
                                 icon=create_icon(os.path.join(src_path, 'install.svg')))
        self.publish_app_button.setEnabled(False)
        self.get_app_button = \
            self.add_tool_button('install_app_button', self.tr('Market'),
                                 icon=create_icon(os.path.join(src_path, 'appstore.svg')))
        self.get_app_button.setEnabled(False)
        self.addSeparator()

        self.apps_panel = pmwidgets.TopLevelWidget(self)

        self.buttons_toolbox = PMGToolBox()
        self.apps_panel.set_central_widget(self.buttons_toolbox)
        self.show_apps_button_bar = PMMenuToolPanel()  # 设置工具条按钮组合
        self.addWidget(self.show_apps_button_bar)

        if platform.system().lower() == 'windows':
            from pyminer2.globals import getPythonModulesDirectory

            dir = os.path.join(getPythonModulesDirectory(), 'qt5_applications', 'Qt', 'bin')

            self.designer_path = os.path.join(dir, 'designer.exe').replace('/', '\\')
            self.pyuic_path = os.path.join(dir, 'designer.exe').replace('/', '\\')
            self.linguist_path = os.path.join(dir, 'linguist.exe').replace('/', '\\')
            self.pyrcc_path = os.path.join(dir, 'designer.exe').replace('/', '\\')
        else:
            dir = ''
            self.designer_path = os.path.join(dir, 'designer.exe')
            self.pyuic_path = os.path.join(dir, 'designer.exe')
            self.linguist_path = os.path.join(dir, 'linguist.exe')
            self.pyrcc_path = os.path.join(dir, 'designer.exe')

    def get_toolbar_text(self) -> str:
        return self.tr('Applications')

    def on_close(self):
        self.hide()
        self.deleteLater()

    def set_panel_visibility(self):
        self.refresh_pos()

        self.apps_panel.setVisible(not self.apps_panel.isVisible())

    def refresh_pos(self):
        """
        刷新顶上的ToplevelWidget的位置。
        """
        # btn = self.get_control_widget('button_show_more_plots')
        #
        # width = self.get_control_widget('button_list').width()
        # self.apps_panel.set_width(width)
        # self.apps_panel.set_position(QPoint(btn.x() - width, btn.y()))

    def main_appstore_dispaly(self):
        """
        显示"应用商店"窗口
        """
        self.appstore = base.AppstoreForm()
        # self.import_database.signal_data_change.connect(self.slot_dataset_reload)
        self.appstore.show()

    def bind_events(self):
        """
        绑定事件。这个将在界面加载完成之后被调用。
        """
        self.extension_lib.Signal.get_window_geometry_changed_signal().connect(self.refresh_pos)
        self.extension_lib.Signal.get_close_signal().connect(self.on_close)
        self.open_qtdesigner_button.clicked.connect(self.open_designer)

        # self.packup_app_button.clicked.connect(self.open_app_wizard)

        menu_open_wizard = self.append_menu('packup_app_button', self.tr('New Project'), self.open_app_wizard)
        menu_open_wizard.setToolTip(self.tr('New Project'))
        menu_open_wizard.triggered.connect(self.open_app_wizard)
        self.append_menu('packup_app_button', self.tr('Open Translator'), self.open_translator)
        self.append_menu('packup_app_button', self.tr('Run PyLUpdate'), self.run_pylupdate)
        self.append_menu('packup_app_button', self.tr('Run PyUIC'), self.run_pyuic)
        self.append_menu('packup_app_button', self.tr('Run PyRCC'), self.run_pyrcc)

        self.publish_app_button.clicked.connect(self.open_app_publish)
        self.get_app_button.clicked.connect(self.open_app_store)

    def open_translator(self):

        if platform.system() == "Windows":
            if os.path.exists(self.linguist_path):
                print(self.linguist_path, type(self.linguist_path))
                subprocess.Popen(self.linguist_path)
            else:
                subprocess.Popen(['pyqt5designer'])
        else:
            subprocess.Popen(['designer'])

    def open_app_wizard(self):
        self.wizard = app_designer_wizard()
        self.wizard.extension_lib = self.extension_lib
        print(self.extension_lib)
        self.wizard.show()

    def open_app_publish(self):
        print("你点击了应用发布")

    def open_app_store(self):
        print("你点击了应用商店")

    def open_in_designer(self, file_path: str):
        """
        打开qtDesigner进行ui编辑
        Returns:

        """
        import subprocess
        import platform
        from pyminer2.globals import getPythonModulesDirectory
        if platform.system() == "Windows":
            dir = os.path.join(getPythonModulesDirectory(), 'qt5_applications/Qt/bin')
            path = os.path.join(dir, 'designer.exe')
            if os.path.exists(path):
                subprocess.Popen([path, file_path], cwd=dir)  # 打开中文版designer。
            else:
                subprocess.Popen(['pyqt5designer'])
        else:
            subprocess.Popen(['designer'])

    def open_in_linguist(self, file_path: str):
        if platform.system() == "Windows":
            if os.path.exists(self.linguist_path):
                print(self.linguist_path, type(self.linguist_path))
                subprocess.Popen([self.linguist_path, file_path])
            else:
                subprocess.Popen(['pyqt5designer'])
        else:
            subprocess.Popen(['designer'])

    def open_designer(self):
        """
        打开qtDesigner进行ui编辑
        Returns:

        """
        import subprocess
        import platform
        from pyminer2.globals import getPythonModulesDirectory
        if platform.system() == "Windows":
            dir = os.path.join(getPythonModulesDirectory(), 'qt5_applications/Qt/bin')
            path = os.path.join(dir, 'designer.exe')
            if os.path.exists(path):
                subprocess.Popen(path, cwd=dir)  # 打开中文版designer。
            else:
                subprocess.Popen(['pyqt5designer'])
        else:
            subprocess.Popen(['designer'])

    def run_pyuic(self):
        ret = QMessageBox.warning(self, self.tr('Warning'),
                                  self.tr(
                                      'All .ui files in working directory will be converted to python file and all generated python'
                                      'files will be overwritten.\nStill continue?'),
                                  QMessageBox.Ok | QMessageBox.Cancel
                                  , QMessageBox.Cancel)
        if ret == QMessageBox.Ok:
            work_dir = self.extension_lib.Program.get_settings()['work_dir']
            ui_files = self.list_files(work_dir, '.ui')
            for ui_file_path in ui_files:
                try:
                    pyname = os.path.splitext(os.path.basename(ui_file_path))[0] + '.py'
                    pypath = os.path.join(os.path.dirname(ui_file_path), pyname)
                    os.system('%s -m PyQt5.uic.pyuic -x "%s" -o "%s" ' % (sys.executable, ui_file_path, pypath))
                except Exception as e:
                    import traceback
                    traceback.print_exc()

    def run_pylupdate(self):
        work_dir = self.extension_lib.Program.get_settings()['work_dir']
        cmd = '%s -m PyQt5.pylupdate_main -noobsolete' % sys.executable

        ret = QMessageBox.warning(self, self.tr('Warning'),
                                  self.tr(
                                      'All .py files in working directory with tr() or translate() functions will be converted and .ts file will be updated.\n'
                                      'Still continue?'),
                                  QMessageBox.Ok | QMessageBox.Cancel
                                  , QMessageBox.Cancel)
        if ret == QMessageBox.Ok:
            ts_files = self.list_files(work_dir, '.ts')
            py_files = self.list_files(work_dir, '.py')
            if len(ts_files) != 1:
                QMessageBox.warning(self, self.tr('Warning'), self.tr('No or More than 1 .ts files!'))
                return
            for file_name in py_files:
                try:

                    pypath = os.path.join(work_dir, file_name)
                    with open(pypath, 'rb') as f:
                        s = f.read()
                        if b'tr' in s or b'translate' in s:
                            cmd += ' ' + pypath + ' '
                except Exception as e:
                    import traceback
                    traceback.print_exc()
            cmd += ' -ts %s' % ts_files[0]
            print('ts', cmd)
            os.system(cmd)

    def run_pyrcc(self):
        """
        运行Pyrcc
        Returns:

        """
        ret = QMessageBox.warning(self, self.tr('Warning'),
                                  self.tr(
                                      'All .qrc files in working directory will be converted to *_rc.py and all generated python'
                                      'files will be overwritten.\nStill continue?'),
                                  QMessageBox.Ok | QMessageBox.Cancel
                                  , QMessageBox.Cancel)
        if ret == QMessageBox.Ok:
            cmd = '%s -m PyQt5.pyrcc_main' % sys.executable
            work_dir = self.extension_lib.Program.get_settings()['work_dir']
            qrcs = self.list_files(work_dir, '.qrc')
            for qrc_path in qrcs:
                try:
                    py_path = os.path.join(os.path.splitext(qrc_path)[0] + '_rc.py')
                    os.system(cmd + ' ' + '%s -o %s' % (qrc_path, py_path))
                except:
                    import traceback
                    traceback.print_exc()

    def refresh_outer_buttons(self):
        """
        刷新显示在按纽条上面的按钮们。
        首先全部移除，然后添加进来。
        这些按钮不是由用户控制添加的，而是自动的呈现listwidget的前最多10项。
        """
        return

    def add_toolbox_widget(self, group: str, text: str,
                           icon_path: str, action: Callable, hint: str = '', refresh=True):
        """
        将控件添加到工具箱，成为QListWidgetItem，通过这些Item生成对应的按钮。
        """
        tb = self.buttons_toolbox.add_button(group, text, icon_path, action)
        if refresh:
            self.refresh_outer_buttons()

    def list_files(self, folder: str, filter_ext: str) -> List[str]:
        """
        Args:
            folder:
            filter_ext:

        Returns:

        """
        files_list = []
        if not filter_ext.startswith('.'):
            filter_ext = '.' + filter_ext

        for home, dirs, files in os.walk(folder):
            for filename in files:
                if filename.endswith(filter_ext):
                    file_path = os.path.join(home, filename)
                    files_list.append(file_path)

        return files_list


class app_designer_wizard(QWizard, Ui_Wizard):
    """
    应用设计引导，通过多步骤引导用户开发扩展应用
    """

    def __init__(self):
        super(app_designer_wizard, self).__init__()
        self.extension_lib = None
        self.setupUi(self)

        self.app_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 扩展应用所在位置
        self.current_app_path = os.path.join(self.app_path, self.app_name.text())  # 当前扩展路径,需要初始化一下防止出现问题。
        # self.finished.connect(self.on_finished)
        self.currentIdChanged.connect(self.package_check)
        self.intruduce.completeChanged.connect(self.package_check)
        self.btn_qtdesigner.clicked.connect(self.open_designer)
        self.icon_choose.clicked.connect(self.icon_path_choose)
        self.app_name.textChanged.connect(self.name_check)
        self.btn_open_folder.clicked.connect(self.open_app_folder)

        # 设置插件默认图标
        if len(self.icon_path.text()) == 0:
            icon_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'source\default.png')
            self.icon_path.setText(icon_path)

        # 设置默认应用描述
        self.app_desciption.setText("自定义开发的应用")

    def validateCurrentPage(self) -> bool:
        """
        点击Next或者Finish的时候可以触发。
        点击Finish的时候，切换当前的路径到创建的文件夹中，
        :return:
        """

        print('id', self.currentId())
        if self.currentId() == 3 and self.extension_lib is not None:
            self.extension_lib.Program.set_work_dir(self.current_app_path)
            self.extension_lib.Signal.get_settings_changed_signal().emit()
        return super(app_designer_wizard, self).validateCurrentPage()

    def name_check(self):
        """
        检查是否已存在同名插件应用，如果存在则进行提示
        """
        app_name = self.app_name.text()  # 应用名称
        self.current_app_path = os.path.join(self.app_path, app_name)

        if os.path.exists(self.current_app_path) and len(self.app_name.text()) > 0:
            QMessageBox.information(self, '提示', '相同名称的扩展应用已存在!\n目录：' + self.app_path + '\n名称：' + app_name)

    def icon_path_choose(self):
        """
        选择插件应用的图标文件
        """
        path = ''
        if self.extension_lib is not None:
            path = os.path.dirname(self.extension_lib.Program.get_main_program_dir())
            path = os.path.join(path, 'pmtoolbox', 'ui', 'src')

        file_path, filetype = QFileDialog.getOpenFileName(self,
                                                          '选择文件', path,
                                                          "图片文件 (*.icon *.png *.jpg *.svg *.jpeg)")

        if len(file_path) > 0:
            self.icon_path.setText(file_path)

    def package_check(self):
        """
        根据用户在引导页面填写的信息，生成应用文件夹，并填充应用json内容
        Returns:

        """
        print(self.currentPage().title(), self.currentPage().nextId())
        if self.currentPage().nextId() == 3:
            print(self.currentPage().objectName())

            app_name = self.app_name.text()  # 应用名称
            app_display_name = self.app_display_name.text()  # 显示名称
            author = self.author.text()  # 作者
            version = self.version.text()  # 版本号
            icon_path = self.icon_path.text()  # 应用图标
            app_type = self.app_type.currentText()  # 应用类型
            app_class = self.app_class.currentText()  # 分类

            # 应用描述 如果应用描述为空，则默认应用描述等于应用名称
            if len(self.app_desciption.toPlainText()) > 0:
                app_desc = self.app_desciption.toPlainText()
            else:
                app_desc = app_name

            # 检查路径是否已存在，如果存在则进行提示，否则创建
            if os.path.exists(self.current_app_path) and len(self.app_name.text()) > 0:
                QMessageBox.information(self, '提示', '相同名称的扩展应用已存在!\n目录：' + self.app_path + '\n名称：' + app_name)
            else:
                os.mkdir(self.current_app_path)

            self.app_entrance.setText(app_name + '.py')  # 设置应用入口

            # 复制图片到应用目录 如果用户未选择图标文件则指定默认图标
            if len(self.icon_path.text()) > 0:
                icon_name = os.path.basename(icon_path)
                shutil.copy(icon_path, os.path.join(self.current_app_path, icon_name))
            else:
                icon_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'source\default.png')
                icon_name = 'default.png'
                shutil.copy(icon_path, os.path.join(self.current_app_path, icon_name))

            # 整理package.json
            package_dict = {'name': app_name, 'display_name': app_display_name, 'author': author, 'version': version,
                            'description': 'demo',
                            'icon': icon_name, 'interface': {'file': 'main.py', 'interface': 'Interface'},
                            'widgets': [], 'requirements': [], 'settings': 'settings.json'}
            package_json = json.dumps(package_dict, ensure_ascii=False, indent=2)
            # 生成package.json 到应用目录
            with open(os.path.join(self.current_app_path, 'package.json'), 'w', encoding='utf-8') as file:
                file.write(package_json)

            # 生成配置信息setting.json到应用目录
            setting_dict = {"locale": "zh-CN", "color": "white"}
            with open(os.path.join(self.current_app_path, 'settings.json'), 'w', encoding='utf-8') as file:
                file.write(json.dumps(setting_dict, ensure_ascii=False, indent=2))

            # 生成 main.py到应用目录
            main_py = ''
            main_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'source/main.py')
            for line in open(main_path, 'r', encoding='utf-8'):
                if line.find("作者") >= 0:
                    line = line.replace(line.split(':')[1], author + '\n')
                if line.find("创建日期") >= 0:
                    import time
                    now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                    line = line.replace(line.split(':')[1], now_time + '\n')
                if line.find("说明") >= 0:
                    line = line.replace(line.split(':')[1], app_desc + '\n')
                if line.find("应用分类") >= 0:
                    line = line.replace('应用分类', app_class)
                if line.find("应用名称") >= 0:
                    line = line.replace('应用名称', app_name)
                if line.find("应用图标") >= 0:
                    line = line.replace('应用图标', icon_name)
                if line.find("入口文件") >= 0:
                    line = line.replace('入口文件', app_name + '.py')
                if len(line) > 0:
                    main_py = main_py + line

            with open(os.path.join(self.current_app_path, 'main.py'), 'w', encoding='utf-8') as f:
                f.write(main_py)

            # 写入执行文件
            run_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'source/run.py')
            run_target = os.path.join(self.current_app_path, app_name + '.py')
            print(run_target)
            shutil.copy(run_path, run_target)

    def open_designer(self):
        """
        打开qtDesigner进行ui编辑
        Returns:

        """
        import subprocess
        import platform
        if platform.system() == "Windows":
            subprocess.Popen(['pyqt5designer'])
        else:
            subprocess.Popen(['designer'])

    def open_app_folder(self):
        """
        使用资源管理器打开应用所在目录
        Returns:

        """
        print(self.current_app_path)
        os.startfile(self.current_app_path)


if __name__ == '__main__':
    s = PMApplicationsToolBar.list_files(None, r'c:\users\12957\desktop', '.md')
    print(s)
