<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>PMApplicationsToolBar</name>
    <message>
        <location filename="../applications_toolbar.py" line="120"/>
        <source>Designer</source>
        <translation>界面设计</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="123"/>
        <source>Develop</source>
        <translation>应用开发</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="127"/>
        <source>Distribute</source>
        <translation>应用发布</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="131"/>
        <source>Market</source>
        <translation>应用市场</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="160"/>
        <source>Applications</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="200"/>
        <source>New Project</source>
        <translation>新建项目</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="202"/>
        <source>Open Translator</source>
        <translation>打开翻译程序</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="203"/>
        <source>Run PyLUpdate</source>
        <translation>更新翻译文件</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="204"/>
        <source>Run PyUIC</source>
        <translation>更新界面文件</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="205"/>
        <source>Run PyRCC</source>
        <translation>更新资源文件</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="337"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="282"/>
        <source>All .ui files in working directory will be converted to python file and all generated pythonfiles will be overwritten.
Still continue?</source>
        <translation>工作路径下全部.ui文件都要被转换为Python文件，之前生成的全部Python文件将被覆盖。
是否继续？</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="304"/>
        <source>All .py files in working directory with tr() or translate() functions will be converted and .ts file will be updated.
Still continue?</source>
        <translation>工作路径下全部包含tr()和translate()的.py文件都要被转换为.ts文件，且之前生成的.ts文件将被覆盖。
是否继续？</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="314"/>
        <source>No or More than 1 .ts files!</source>
        <translation>路径下不存在.ts文件或者多于一个.ts文件!</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="337"/>
        <source>All .qrc files in working directory will be converted to *_rc.py and all generated pythonfiles will be overwritten.
Still continue?</source>
        <translation>工作路径下全部.qrc文件都要被转换同名并加上_rc后缀的Python文件，之前生成的Python资源文件将被覆盖。
是否继续？</translation>
    </message>
</context>
<context>
    <name>PMProcessConsoleTabWidget</name>
    <message>
        <location filename="../process_monitor.py" line="53"/>
        <source>Terminate</source>
        <translation>终止</translation>
    </message>
    <message>
        <location filename="../process_monitor.py" line="53"/>
        <source>Process is running, Would you like to terminate this process?</source>
        <translation>进程正在运行，你希望终止进程吗?</translation>
    </message>
    <message>
        <location filename="../process_monitor.py" line="15"/>
        <source>Run</source>
        <translation>运行</translation>
    </message>
</context>
<context>
    <name>Wizard</name>
    <message>
        <location filename="../ui/app_designer.py" line="190"/>
        <source>demo_app</source>
        <translation>demo_app</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="192"/>
        <source>demo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="195"/>
        <source>1.0.1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="202"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="213"/>
        <source>main.py</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="215"/>
        <source>demo.py</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="174"/>
        <source>App Developing Wizard</source>
        <translation>应用开发向导</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="175"/>
        <source>Introduction</source>
        <translation>介绍</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="176"/>
        <source>PyMiner makes it easier to develop  applications by PyQt5. 

The applications can only run on your machine or release to application market to used by more users. 

The applications are of two types: Toolbox Application and Embedded Application.

Toolbox Application is suitable for scientific calculation. You can start it by double clicking the buttons in Application toolbar.

Embedded Application will be displayed on PyMiner MainWindow, which strengthens the PyMiner platform.

Let&apos;s start our tour on developing Applications! 
</source>
        <translation>    PyMiner提供了应用开发功能，在这里用户可以自由、方便的开发属于自己的强大应用！这些应用可以在本地直接作为私有应用使用，也可以发布到应用中心，让更多用户进行体验，准备好了吗？我们正在期待你的作品！

    我们的扩展应用主要分为工具箱应用和侧边栏应用。

    其中，工具箱应用可以在主菜单的“应用”中进行调用并弹窗进行操作；而从侧边栏应用则显示在主程序的侧边栏，可以通过点击其中按钮进行对应的操作。

    下面，让我们试着开发一个插件吧！</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="188"/>
        <source>App Information</source>
        <translation>应用信息</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="189"/>
        <source>App Name:</source>
        <translation>应用名称：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="191"/>
        <source>Author:</source>
        <translation>作者：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="193"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="196"/>
        <source>App Type:</source>
        <translation>应用类型：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="197"/>
        <source>Toolbox Application</source>
        <translation>工具箱应用</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="198"/>
        <source>Embedded Application</source>
        <translation>PyMiner扩展应用</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="199"/>
        <source>Text:</source>
        <translation>文字：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="200"/>
        <source>Test Application</source>
        <translation>测试应用</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="201"/>
        <source>Icon:</source>
        <translation>图标：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="203"/>
        <source>Description:</source>
        <translation>描述：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="204"/>
        <source>Field:</source>
        <translation>应用领域：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="205"/>
        <source>Math&amp;Statistics</source>
        <translation>数学与统计</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="206"/>
        <source>Economics&amp;Finance</source>
        <translation>经济与金融</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="207"/>
        <source>Electronics&amp;Communication Engineering</source>
        <translation>电子与通信</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="208"/>
        <source>Biology&amp;Medical</source>
        <translation>生物和医药</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="209"/>
        <source>Civil Engineerings</source>
        <translation>土木工程</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="210"/>
        <source>Mechanical Engineerings</source>
        <translation>机械工程</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="211"/>
        <source>Start Designing</source>
        <translation>开始设计</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="214"/>
        <source>Main Program:</source>
        <translation>主程序：</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="216"/>
        <source>Well done! Now we have created the project folder, and your application is right here.

However your application needs a GUI with some code. In PyMiner, we develop GUI with Qt Designer and write code in the editor of PyMiner.</source>
        <translation>你做的很好！现在我们已经在pyminer/pyminer2/extensions/packages目录下新建了文件夹，属于你的应用就放在这里！

现在你的应用还不能进行实际操作，要开发一个具有完整功能的插件，你的插件还需要操作界面和一些代码，在PyMiner中，我们使用Qt设计师开发应用的界面，使用python编写应用代码。</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="219"/>
        <source>Qt Designer</source>
        <translation>Qt设计师</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="220"/>
        <source>Now your applications has been initialized, and you can enter the App folder to develop it.</source>
        <translation>现在，你的应用已经初始化了，你可以进入目录中继续完善你的应用。</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="221"/>
        <source>Open App Folder</source>
        <translation>打开应用文件夹</translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="194"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;版本号格式请参考：x.y.z&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/app_designer.py" line="212"/>
        <source>Entry：</source>
        <translation></translation>
    </message>
</context>
</TS>
