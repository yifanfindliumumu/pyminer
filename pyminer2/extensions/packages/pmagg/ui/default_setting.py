# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'default_setting.ui'
#
# Created by: PyQt5 UI code generator 5.15.1
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(450, 365)
        Form.setMinimumSize(QtCore.QSize(450, 365))
        Form.setMaximumSize(QtCore.QSize(450, 365))
        self.gridLayout_2 = QtWidgets.QGridLayout(Form)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.comboBox_14 = QtWidgets.QComboBox(Form)
        self.comboBox_14.setObjectName("comboBox_14")
        self.gridLayout.addWidget(self.comboBox_14, 6, 1, 1, 1)
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.comboBox_gui_language = QtWidgets.QComboBox(Form)
        self.comboBox_gui_language.setObjectName("comboBox_gui_language")
        self.gridLayout.addWidget(self.comboBox_gui_language, 0, 1, 1, 1)
        self.label_44 = QtWidgets.QLabel(Form)
        self.label_44.setStatusTip("")
        self.label_44.setWhatsThis("")
        self.label_44.setObjectName("label_44")
        self.gridLayout.addWidget(self.label_44, 6, 0, 1, 1)
        self.comboBox_3 = QtWidgets.QComboBox(Form)
        self.comboBox_3.setObjectName("comboBox_3")
        self.gridLayout.addWidget(self.comboBox_3, 3, 1, 1, 1)
        self.comboBox = QtWidgets.QComboBox(Form)
        self.comboBox.setObjectName("comboBox")
        self.gridLayout.addWidget(self.comboBox, 1, 1, 1, 1)
        self.label_46 = QtWidgets.QLabel(Form)
        self.label_46.setObjectName("label_46")
        self.gridLayout.addWidget(self.label_46, 5, 0, 1, 1)
        self.comboBox_2 = QtWidgets.QComboBox(Form)
        self.comboBox_2.setObjectName("comboBox_2")
        self.gridLayout.addWidget(self.comboBox_2, 2, 1, 1, 1)
        self.label_17 = QtWidgets.QLabel(Form)
        self.label_17.setObjectName("label_17")
        self.gridLayout.addWidget(self.label_17, 2, 0, 1, 1)
        self.label_26 = QtWidgets.QLabel(Form)
        self.label_26.setObjectName("label_26")
        self.gridLayout.addWidget(self.label_26, 4, 0, 1, 1)
        self.label_47 = QtWidgets.QLabel(Form)
        self.label_47.setObjectName("label_47")
        self.gridLayout.addWidget(self.label_47, 7, 0, 1, 1)
        self.label_18 = QtWidgets.QLabel(Form)
        self.label_18.setObjectName("label_18")
        self.gridLayout.addWidget(self.label_18, 3, 0, 1, 1)
        self.label_16 = QtWidgets.QLabel(Form)
        self.label_16.setObjectName("label_16")
        self.gridLayout.addWidget(self.label_16, 1, 0, 1, 1)
        self.checkBox_clear_history = QtWidgets.QCheckBox(Form)
        self.checkBox_clear_history.setText("")
        self.checkBox_clear_history.setChecked(True)
        self.checkBox_clear_history.setObjectName("checkBox_clear_history")
        self.gridLayout.addWidget(self.checkBox_clear_history, 7, 1, 1, 1)
        self.fontComboBox = QtWidgets.QFontComboBox(Form)
        self.fontComboBox.setObjectName("fontComboBox")
        self.gridLayout.addWidget(self.fontComboBox, 5, 1, 1, 1)
        self.comboBox_13 = QtWidgets.QComboBox(Form)
        self.comboBox_13.setObjectName("comboBox_13")
        self.gridLayout.addWidget(self.comboBox_13, 4, 1, 1, 1)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.pushButton = QtWidgets.QPushButton(Form)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_8.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(Form)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_8.addWidget(self.pushButton_2)
        self.pushButton_3 = QtWidgets.QPushButton(Form)
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout_8.addWidget(self.pushButton_3)
        self.gridLayout.addLayout(self.horizontalLayout_8, 8, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 2, 4)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label.setText(_translate("Form", "系统语言:"))
        self.label_44.setToolTip(_translate("Form", "<html><head/><body><p>绘图风格采用SciencePlots包，只会在重新plt.show()后才会生效，文字中含有 中文可能会报错，默认字体的设置将会无效</p></body></html>"))
        self.label_44.setText(_translate("Form", "选择默认绘图风格："))
        self.label_46.setText(_translate("Form", "切换界面字体"))
        self.label_17.setText(_translate("Form", "英文字体："))
        self.label_26.setText(_translate("Form", "切换Tab页绘图方式："))
        self.label_47.setText(_translate("Form", "关闭窗口清除历史绘图："))
        self.label_18.setText(_translate("Form", "中英文混合字体："))
        self.label_16.setText(_translate("Form", "中文字体："))
        self.pushButton.setText(_translate("Form", "确定"))
        self.pushButton_2.setText(_translate("Form", "取消"))
        self.pushButton_3.setText(_translate("Form", "应用"))
