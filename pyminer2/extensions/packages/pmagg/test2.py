if __name__ == '__main__':
    from PMAgg import Window
    from PyQt5 import QtGui,QtCore
    import os
    import sys
    current_path=os.getcwd()
    sys.path.append(current_path)
    import matplotlib
    import matplotlib.pyplot as plt
    matplotlib.use('module://PMAgg')

    app=Window()
    plt.plot([1,2,3],[4,5,6])
    app.get_canvas(plt.gcf())
    app.show()