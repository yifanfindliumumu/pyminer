from pyminer_algorithms import *
import pytest


def test_communication():
    x = array([1, 2, 3, 4, 5])
    set_var('x', x)
    x = get_var('x')
    print(x)


def test_get_error():
    """
    在没有y的情况下测试,应当出现异常:
    ValueError: variable 'y' not found!
    :return:
    """
    y = get_var('y')
