from flask import Flask
from flask import request
from pmlocalserver.datatransfer.encode_decode import dict_to_b64, b64_to_dict, dict_to_pickle
from multiprocessing import shared_memory
import numpy as np
import cloudpickle

a = 123
b = 456

if __name__ == '__main__':
    server = Flask('ipython_data_server')


    @server.route('/')
    def index():
        user_agent = request.headers.get('User_Agent')
        return 'user_agent is %s' % user_agent


    @server.route('/get_data')
    def run():
        return ''


    @server.route('/start_share_data')
    def start_share_data():
        print('share_data')
        global shm_a
        # x = 1
        import time
        x = np.random.rand(1000, 100, 100)
        t0 = time.time()
        dmp = cloudpickle.dumps(x)
        t1 = time.time()
        print(t1 - t0)
        shm_a = shared_memory.SharedMemory(create=True, size=len(dmp))
        buffer = shm_a.buf
        buffer[:] = dmp
        print(buffer)
        return shm_a.name


    @server.route('/stop_share_data')
    def stop_share_data():
        global shm_a
        shm_a.close()
        shm_a.unlink()
        return 'succeeded!%s closed!' % shm_a.name


    @server.route('/start_share_variables')
    def start_share_variables():
        global shm_a
        a = 1
        b = 2
        msg = request.args.get('msg')
        var_names = b64_to_dict(msg).get('var_names')
        vars = {k: globals().get(k) for k in var_names}
        dmp = dict_to_pickle(vars)
        shm_a = shared_memory.SharedMemory(create=True, size=len(dmp))
        buffer = shm_a.buf
        buffer[:] = dmp
        return shm_a.name


    @server.route('/end_share_variables')
    def stop_share_variables():
        global shm_a
        shm_a.close()
        shm_a.unlink()
        return 'succeeded!%s closed!' % shm_a.name


    server.run(host='127.0.0.1', port=52346, debug=False, threaded=True)
