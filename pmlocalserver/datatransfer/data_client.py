import requests
import time
import socket
from multiprocessing import shared_memory
import typing
from pmlocalserver.datatransfer import dict_to_b64, b64_to_dict, pickle_to_dict, dict_to_pickle
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get(method: str, msg: str) -> bytes:
    s = socket.socket()
    s.connect(('localhost', 52346))

    s.sendall(('''GET %s?msg=%s HTTP/1.1\r\n\r\n''' % (method, msg)).encode('ascii'))
    bytes_list = []
    buf = s.recv(1024)
    while len(buf):
        buf = s.recv(1024)
        bytes_list.append(buf)
    logger.info('got message,length:%d' % len(bytes_list))
    return b''.join(bytes_list).split(b'\r\n\r\n', maxsplit=1)[1]


def set_vars(vars: typing.Dict[str, typing.Any]):
    msg = get('/set_data', dict_to_b64(vars))
    return msg


def get_vars(var_names: typing.List[str], preview=False) -> typing.Dict[str, typing.Any]:
    var_b64 = get('/get_data', dict_to_b64({'var_names': var_names, 'preview': preview}))
    msg = b64_to_dict(var_b64)
    return msg


def get_var_names(filter: str = '') -> typing.List:
    print(filter)
    msg = get('/get_var_names', dict_to_b64({'type_filter': filter}))
    var_names = b64_to_dict(msg).get('var_names')
    return var_names


def get_shared_variables(var_names):
    shm_name = get('/start_share_variables', dict_to_b64({'var_names': var_names}))
    logger.info('get shared variables %s from shmname%s' % (var_names, shm_name))
    from multiprocessing import shared_memory
    shm_b = shared_memory.SharedMemory(shm_name.decode(encoding='utf8'))
    b = shm_b.buf.tobytes()
    df = pickle_to_dict(b)
    shm_b.close()  # Close each SharedMemory instance
    response = get('/end_share_variables', shm_name.decode(encoding='utf8'))
    print(response)
    # print(df)
    return df


def set_shared_variables(var_dic):
    dmp = dict_to_pickle(var_dic)
    shm_a = shared_memory.SharedMemory(create=True, size=len(dmp))
    buffer = shm_a.buf
    buffer[:] = dmp
    response = get('/set_variables_shared', shm_a.name)
    shm_a.close()
    shm_a.unlink()


def delete_variables(var_names):
    var_b64 = get('/delete_variables', dict_to_b64({'var_names': var_names}))
    # msg = b64_to_dict(var_b64)
    return var_b64


if __name__ == '__main__':
    import numpy as np

    rdm = np.random.rand(1000, 1000, 100)
    t0 = time.time()
    # res = get('/get_data')
    st = set_shared_variables({'a': 123, 'b': 456, 'c': rdm})
    # st = get_vars(['a', 'b'])
    # get_shared_data()
    # get_shared_variables(['a','b'])
    t1 = time.time()
    print(t1 - t0)
